const valid = require('../utils/validation.js')


function validRegister(req,res,next){
    var { error } = valid.validRegister(req.body)
    if(error){
        res.status(400).send(error.details[0].message)
    }else{
        next()
    }
}
function validLogin(req,res,next){
    var { error } = valid.validLogin(req.body)
    if(error){
        res.status(400).send(error.details[0].message)
    }else{
        next()
    }
}



module.exports = {validLogin, validRegister}