const client = require('../config/client.js')

function searchingForUsers(req,res,next){
    client.query('SELECT * FROM users WHERE username =$1',[req.body.username])
    .then(data =>{
        if(data.rows.length === 0){
            next()
        }else{
            next({
                message:'Username Already Exists',
                status: 406
            })
        }
    })
    .catch(err => next(err))
}

module.exports ={searchingForUsers}