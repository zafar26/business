const company = require('../controllers/company.js')
const middle =require('../middleware/errhandler.js')
const verify = require('../utils/verifytokens.js')

var express = require('express');
var router = express.Router();

router.use(express.json())


router.get('/',verify,company.getAllcompany, middle.catchError)

router.get('/:id',verify, middle.companyIdValidator,company.getCompanyById, middle.catchError)

router.post('/',verify, middle.companyValidator, company.postCompanyById, middle.catchError)

router.put('/:id',verify, middle.companyIdValidator, company.putCompanyById, middle.catchError)

router.delete('/:id', verify, middle.companyIdValidator, company.deleteCompanyById, middle.catchError)



module.exports = router;