const search = require('../middleware/extra.js')
const controller = require('../controllers/user.js')
const middle =require('../middleware/errhandler.js')
const mid = require('../middleware/user.js')
const verify = require('../utils/verifytokens.js')
var express = require('express');
var router = express.Router();


router.use(express.json())
router.delete('/users/:username',verify,controller.deleteUser, middle.catchError)

router.put('/users/:id',verify,mid.validRegister,controller.putUser, middle.catchError)


router.get('/users',verify,controller.getRegister, middle.catchError)

router.post('/register', mid.validRegister,search.searchingForUsers, controller.postRegister, middle.catchError)
router.post('/login',mid.validLogin, controller.loginQuery, middle.catchError)


module.exports = router;
