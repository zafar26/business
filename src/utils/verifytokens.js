
require('dotenv').config();
const jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {

    const token = req.header('auth-token');
    if (!token) {
        return res.status(404).send('Access Denied');
    }
    try {
        const verified = jwt.verify(token, process.env.SECRET_TOKEN, {
            expiresIn: '300s'
        });
        next();
    }
    catch (err) {
        res.status(404).send('invalid token');  
    }
};