const client = require('../config/client.js')

async function companyIdValidation(array){
    var company = array.pop()
    var companyId = await client.query('SELECT * FROM company WHERE company_name = $1',[company])
    if(companyId.rowCount === 0){
        return array
    }else{
        array.push(companyId.rows[0].id)
    }
    return array
}

function getAllPerson(){
    return client.query('SELECT * FROM person')
}
function getPersonById(id){
    return client.query('SELECT * FROM person WHERE person_id =$1',[id])
}

function postPersonById(array){
        return client.query('INSERT INTO person(first_name,last_name,country,phone,email,company_id) VALUES($1,$2,$3,$4,$5,$6)',array)
  
}


function putPersonById(id,array){
    array.push(id)
    return client.query('UPDATE person SET first_name = $1,last_name =$2 ,country = $3,phone = $4,email = $5,company_id = $6 WHERE person_id = $7',array)
            
}

function deletePersonById(id){
    return client.query('DELETE FROM person WHERE person_id =$1',[id])
}



module.exports = { deletePersonById, putPersonById, postPersonById, getPersonById, getAllPerson, companyIdValidation }