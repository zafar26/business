var logger = require('./utils/loging.js')
var companyroute = require('./routes/companyroute.js');
var personroute = require('./routes/personroute.js');
var registerroute = require('./routes/userroute.js')
const express = require('express')
const app = express()


app.use(logger.log);

app.use('/', registerroute);
app.use('/company', companyroute);
app.use('/person', personroute);

app.use(logger.error);

app.use('*',(req,res) => {res.status(404).send('Invalid URL')})

app.listen(process.env.PORT, () => console.log(`Listening on port ${process.env.PORT}:`))