const Joi = require('joi')


function numberValidator(id){
    let idObject = {};
    idObject['id'] = id
    const schema = {
        id: Joi.number().integer().less(1000)
    }
    return Joi.validate(idObject, schema)
}


function companyValidation(data){
    const schema={
        company_name: Joi.string().min(6).max(50).required()
    }
    return Joi.validate(data, schema)
}


function validatingData(data){
    const personPostSchema ={
        first_name : Joi.string().min(4).max(50).required(),
        last_name :  Joi.string().min(4).max(50).required(),
        country :  Joi.string().min(2).max(3).required(),
        phone :  Joi.string().min(10).max(16).required(),
        email :  Joi.string().min(6).max(50).required(),
        company_name : Joi.string().min(6).max(50).required()
    }     
    return Joi.validate(data, personPostSchema)
}

function validRegister(data){
    const personPostSchema ={
        first_name : Joi.string().min(4).max(50).required(),
        last_name :  Joi.string().min(4).max(50).required(),
        username :  Joi.string().min(6).max(14).required(),
        password: Joi.string().min(6).max(14).required(),
        phone :  Joi.string().min(10).max(16).required(),
        email :  Joi.string().min(6).max(50).required(),
    
    }     
    return Joi.validate(data, personPostSchema)
}

function validLogin(data){
    const personPostSchema ={
        username :  Joi.string().min(6).max(50).required(),
        password: Joi.string().min(6).required()
    }     
    return Joi.validate(data, personPostSchema)
}

module.exports ={ validatingData, numberValidator, companyValidation, validLogin, validRegister }