var valid = require('../utils/validation.js')
var client = require('../config/client')



function catchError(err, req, res, next) {
    res.status(err.status || 500).send(err.message);
    next(err.message);
}


function companyIdValidator(req,res,next){
    const result = valid.numberValidator(req.params.id)
    if(result.error){
        next({
            message: result.error.details[0].message,
            status: 404
        })
        res.end()
    }else{
        client.query('SELECT * FROM company WHERE id = $1',[req.params.id])
            .then(data=>{
                if(data.rows.length === 0){
                    next({
                        message:'Company Id Not Found In Database',
                        status: 404
                    })
                }else{
                     next()
                }
        })
        .catch(err=>{
            next(err)
        })
    }
}


function personIdValidator(req,res,next){
    const result = valid.numberValidator(req.params.id)
    if(result.error){
        next({
            message: result.error.details[0].message,
            status: 404
        })
        res.end()
    }else{
        client.query('SELECT * FROM person WHERE person_id = $1',[req.params.id])
            .then(data=>{   
                if(data.rows.length === 0){
                    next({
                        message:'Person Id Not Found In Database',
                        status: 404
                    })
                }else{
                next()
                }
            })
            .catch(err=>{
                next(err)
            })
    }
}


function stringValidator(req,res,next){
    const result = valid.validatingData(req.body)
    if (result.error){
        res.status(400).send(result.error.details[0].message)
    }else{
        next()
    }
}

function companyValidator(req,res,next){
    var { error } = valid.companyValidation(req.body)
    if(error){
        res.status(400).send(error.details[0].message)
    }else{
        next()
    }
}

module.exports = { catchError, stringValidator, companyValidator, companyIdValidator, personIdValidator}