const client = require('../config/client.js')

function getRegister(){
    return client.query('SELECT username,user_id FROM users ')
}

function postregister(array){
    return client.query('INSERT INTO users(first_name,last_name,username,password,phone,email) VALUES($1,$2,$3,$4,$5,$6)',array)
}

function loginQuery(username){
    return client.query('SELECT * FROM users WHERE username = $1',[username])
}
function deleteUser(username){
    return client.query('DELETE FROM users WHERE username =$1',[username])
}
function putuser(id,array){
    array.push(id)
    return client.query('UPDATE users SET first_name = $1,last_name =$2 , username = $3, password = $4,phone = $5,email = $6 WHERE user_id = $7',array)
            
}

module.exports = {loginQuery, postregister, getRegister, deleteUser, putuser}