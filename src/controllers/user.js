const model = require('../models/user.js')
var passwordHash = require('password-hash');
const jwt = require('jsonwebtoken')

function getRegister(req, res, next) {
    model.getRegister()
        .then(data => res.status(200).send(data.rows))
        .catch(err => next(err))
}


function postRegister(req, res, next) {
    var array = Object.values(req.body)
    array[3] = passwordHash.generate(req.body.password);
    model.postRegister(array)
        .then(data => res.status(200).send(req.body))
        .catch(err => next(err))
}



function loginQuery(req, res, next) {
    model.loginQuery(req.body.username)
        .then(data => {
            if (data.rows.length !== 0) {
                if (passwordHash.verify(req.body.password, data.rows[0].password)) {
                    const values = Object.values(req.body)
                    const tokens = jwt.sign({ username: req.body.username },
                        process.env.SECRET_TOKEN);

                    res.header('auth-login', tokens).send(tokens);

                } else {
                    next({
                        message: "Password Didn't match",
                        status: 401
                    })
                }
            } else {
                next({
                    message: 'Username not Found',
                    status: 404
                })
            }
        })
        .catch(err => next(err))
}
function deleteUser(req, res, next) {
    model.deleteUser(req.params.username)
        .then(data => res.send('Deleted'))
        .catch(err => next(err))

}
function putUser(req, res, next) {
    var array = Object.values(req.body)
    array[3] = passwordHash.generate(req.body.password);


    model.putuser(req.params.id, array)
        .then(data => res.send(data))
        .catch(err => next(err))
}

module.exports = { getRegister, postRegister, loginQuery, deleteUser, putUser }
