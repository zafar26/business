const model = require('../models/company.js')


function getAllcompany(req, res, next) {
    model.getAllcompany()
        .then(data => res.status(200).send(data.rows))
        .catch(err => next(err))
}


function getCompanyById(req, res, next) {
    model.getCompanyById(req.params.id)
        .then(data => res.status(200).send(data.rows))
        .catch(err => next(err))
}


function postCompanyById(req, res, next) {
    model.postCompanyById(Object.values(req.body))
        .then(res.status(200).send(req.body))
        .catch(err => next(err))
}


function putCompanyById(req, res, next) {

    model.putCompanyById(req.params.id, Object.values(req.body))
        .then(data => res.status(200).send(req.body))
        .catch(err => next(err))
}


function deleteCompanyById(req, res, next) {
    model.deleteCompanyById(req.params.id)
        .then(data => res.status(200).send(req.params.id))
        .catch(err => next(err))
}


module.exports = { deleteCompanyById, putCompanyById, postCompanyById, getCompanyById, getAllcompany }


