const { Pool } = require('pg')

const client = new Pool({
    user:process.env.USER,
    host:process.env.HOST,
    database : process.env.DATABASE,
    port:5432,
    password:process.env.PASSWORD
});

module.exports = client;