const controler = require('../controllers/person.js')
var express = require('express');
var router = express.Router();
const verify = require('../utils/verifytokens.js')
var middle = require('../middleware/errhandler.js')

router.use(express.json())

router.get('/',verify,controler.getAllPerson, middle.catchError)

router.get('/:id',verify, middle.personIdValidator, controler.getPersonById, middle.catchError)

router.post('/', verify, middle.stringValidator, controler.postPersonById, middle.catchError)

router.put('/:id',verify, middle.personIdValidator, middle.stringValidator, controler.putPersonById, middle.catchError)

router.delete('/:id', middle.personIdValidator, controler.deletePersonById, middle.catchError)

//export this router to use in our index.js
module.exports = router;