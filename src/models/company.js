const client = require('../config/client.js')


function getAllcompany(){
    return client.query('SELECT * FROM company')
}

function getCompanyById(id){
    return client.query('SELECT * FROM company WHERE id =$1',[id])
}

function postCompanyById(array){
    return client.query('INSERT INTO company(company_name) VALUES($1)',array)
}

function putCompanyById(id,array){
    array.push(id)
return client.query('UPDATE company SET company_name = $1 WHERE id = $2;',array)
}

function deleteCompanyById(id){
    return client.query('DELETE FROM company WHERE id =$1',[id])
}

module.exports={ deleteCompanyById, putCompanyById, postCompanyById, getCompanyById, getAllcompany}



