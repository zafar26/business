const model = require('../models/person.js')




function getAllPerson(req, res, next) {
    model.getAllPerson()
        .then(data => res.status(200).send(data.rows))
        .catch(err => next(err))
}

function getPersonById(req, res, next) {
    model.getPersonById(req.params.id).then(data => res.status(200).send(data.rows))
        .catch(err => next(err))
}



function postPersonById(req, res, next) {
    model.companyIdValidation(Object.values(req.body))
        .then(data => {
        
            if (data.length === Object.entries(req.body).length) {
                model.postPersonById(data)
                    .then(data => res.status(200).send(req.body))
                    .catch(err => next(err))
            } else {
                next({
                    message: 'Company_name Invalid',
                    status: 404
                })
            }
        }).catch(err => next(err))
}


function putPersonById(req, res, next) {
    model.companyIdValidation(Object.values(req.body))
        .then(data => {
            if (data.length === Object.entries(req.body).length) {
                model.putPersonById(req.params.id, data)
                    .then(data => res.status(200).send(req.body))
                    .catch(err => next(err))
            } else {
                next({
                    message: 'Company_name Invalid',
                    status: 404
                })
            }
        })
}


function deletePersonById(req, res, next) {
    model.deletePersonById(req.params.id)
        .then(data => {
            if (data.command === 'DELETE' && data.rowCount >= 1) {
                res.status(200).send('Succesfully Deleted')
            } else {
                res.status(200).send('Not Deleted')
            }
        })
        .catch(err => next(err))
}


module.exports = { deletePersonById, putPersonById, postPersonById, getPersonById, getAllPerson }